/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myiprecorder;

import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ntro
 */
public class MyIpRecorder {

    static boolean isRestart = false;
    static TrayIcon trayIcon;
    static long timer = 60000;
    static long time = 60;

    public static void deployConf() {
        try {
            File file = new File("conf.txt");    //creates a new file instance  
            FileReader fr = new FileReader(file);   //reads the file  
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream  
            StringBuffer sb = new StringBuffer();    //constructs a string buffer with no characters  
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);      //appends line to string buffer  
            }
            fr.close();    //closes the stream and release the resources  
            timer = Long.valueOf(sb.toString());
            System.out.println("Timer: " + timer);
            time = timer / 1000;
        } catch (Exception ex) {
            System.out.println("Timer: " + timer);
        }
    }

    public MyIpRecorder() {
        deployConf();
        show();

        Thread th = new Thread() {
            public synchronized void run() {
                while (true) {
                    try {
                        Date date = new Date();
                        long time = date.getTime();
                        Timestamp ts = new Timestamp(time);

                        writeFile("my_ip.txt", getIpAdd() + "\t" + ts, true);
                        for (int i = 0; i < MyIpRecorder.time; i++) {
                            Thread.sleep(1000);
                        }
                        Thread.sleep(1000);

                    } catch (Exception e) {
                    }
                }

            }

        };

        th.start();

        Thread thRestart = new Thread() {

            public synchronized void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                        System.out.println(isRestart);
                        if (isRestart) {
                            time = -1;
                            Thread.sleep(1000);
                            time = timer / 1000;
                            isRestart = false;
                        }
                    } catch (Exception e) {
                    }
                }

            }

        };

        thRestart.start();

    }

    public static void show() {
        if (!SystemTray.isSupported()) {
            System.exit(0);
        }
        trayIcon = new TrayIcon(createIcon("/myiprecorder/ip.png", "icon"));
        trayIcon.setToolTip("My Ip");
        final SystemTray tray = SystemTray.getSystemTray();

        final PopupMenu menu = new PopupMenu();

        MenuItem myIp = new MenuItem("My Ip");
        MenuItem options = new MenuItem("Options");
        MenuItem exit = new MenuItem("Exit");

        menu.add(myIp);
        menu.add(options);
        menu.addSeparator();
        menu.add(exit);

        myIp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    InetAddress inetAddress = InetAddress.getLocalHost();
                    System.out.println("IP Address:- " + inetAddress.getHostAddress());
                    System.out.println("Host Name:- " + inetAddress.getHostName());
                    JOptionPane.showMessageDialog(null, "Global IP: "+getIpAdd()+"\nLocal IP: "+inetAddress.getHostAddress()
                    +"\nHost Name: "+inetAddress.getHostName());
                } catch (UnknownHostException ex) {
                    
                }
            }
        });
        options.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OptionsFrame optionsFrame = new OptionsFrame();
                optionsFrame.show();
            }
        });
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        trayIcon.setPopupMenu(menu);
        try {
            tray.add(trayIcon);
        } catch (Exception ex) {

        }
    }

    protected static Image createIcon(String path, String desc) {
        URL imageUrl = MyIpRecorder.class.getResource(path);
        return (new ImageIcon(imageUrl, desc)).getImage();
    }

    public static String getIpAdd() {
        String ip = null;
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));

            ip = in.readLine(); //you get the IP as a String
            System.out.println(ip);
        } catch (Exception ex) {

        }
        return ip;
    }

    public static boolean createFile(String file_path) {
        boolean result = true;
        try {
            File myObj = new File(file_path);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (Exception e) {
            System.out.println("An error occurred.");
            result = false;
        }
        return result;
    }

    public static boolean writeFile(String file_path, String mess, boolean isAppend) {
        boolean result = true;
        boolean isCreated = createFile(file_path);
        if (isCreated) {
            try {
                FileWriter myWriter = new FileWriter(file_path, isAppend);
                myWriter.write(mess + "\n");
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            } catch (Exception e) {
                System.out.println("An error occurred.");
                result = false;
            }
        } else {
            result = false;
        }
        return result;
    }

}
